import sys
import pymongo
from pymongo import MongoClient
import pymongo.errors
from pymongo.errors import ConnectionFailure

def main():
    try:
        connect=MongoClient(host="tgl-mongodb22.rctanalytics.com", port=27017)
        print("Connected to Mongo DB successfully")
    except ConnectionFailure:
        print("Cannot connect to Mongo DB")

if __name__ == "__main__":
    main()
