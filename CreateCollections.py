import sys
import pymongo
from pymongo import MongoClient
import pymongo.errors
from pymongo.errors import ConnectionFailure

def main():
    try:
        Connect=MongoClient(host="localhost", port=27017)
        DB=Connect['PythonDatabase']
        collect=DB['Employee']
        dict={"name":"John","company":"Eurofins","City":"Bangalore","DOJ":"10-06-2010"}
        collect.insert_one(dict)
        print("record Inserted successfully")
    except ConnectionFailure :
        print("Not connected to Mongo db")


if __name__ == "__main__":
    main()

